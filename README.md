# dlipr20 exercise3

RWTH Aachen Deep Learning for Physics Research SS20
Student group: bcorneglio stafl98 irratzo

exercise3 MNIST

Final experiments and reports for this exercise get archived in a [dedicated comet.ml workspace](https://www.comet.ml/irratzo). The report is archived under `reports/comet_notes.md`.

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [dlipr20 exercise3](#dlipr20-exercise3)
- [For developers](#for-developers)
    - [Project structure](#project-structure)
    - [Git How-To](#git-how-to)

<!-- markdown-toc end -->


# For developers #

## Project structure

The folder structure is a simplified version of the [cookiecutter data science](https://drivendata.github.io/cookiecutter-data-science/). To create a new project (e.g. exercise), create repo, clone, copy-paste this template inside, and push.

## Git How-To ##

The git How-to for project work can be found [here](https://git.rwth-aachen.de/sisc/dlipr20/git_howto/-/blob/master/README.md).
