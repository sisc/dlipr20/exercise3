# note: this code for ex3 task1 uses data from tf.keras.datasets.mnist instead 
# of dlipr.mnist.
# both data dlipr.mnist and tf.keras.datasets.mnist are identical.
# for details, see project notes at comet.ml/irratzo/exercise3/notes.

# ==============================================================================
# Constants, Imports
# ==============================================================================
# constants
COMET_EXPERIMENT = True
NOTEBOOK = False
REFERENCES = [
    "https://github.com/AviatorMoser/keras-mnist-tutorial", # [1]
    "https://github.com/DavidWalz/dlipr/blob/master/dlipr/utils.py", # [2]
    ]

# setup comet
# for notebook (eg google colab). only needed if comet_ml not installed yet.
if COMET_EXPERIMENT and NOTEBOOK:
    pass # if notebook, uncomment:
    # %pip install comet_ml
# after installation for first time, restart notebook!

if COMET_EXPERIMENT:
    # import comet_ml in the top of your file    
    from comet_ml import Experiment
    
    # Add the following code anywhere in your machine learning file
    experiment = Experiment(api_key="E7YQLfWBuQZXjw0c2dKsXKPsY",
                            project_name="exercise3", workspace="irratzo")
  
def printcomet(string : str = ""):
    # append references (like for "... [1] ..." and so on)
    reflabl = ["[{}]".format(i+1) for i in range(len(REFERENCES))]
    if any([labl in string for labl in reflabl]):
      string += "\nReferences:"
      for i,labl in enumerate(reflabl):
        if labl in string:
          string += "\n{} {}".format(labl, REFERENCES[i])
    
    # print and log
    print(string)
    if COMET_EXPERIMENT:
        experiment.log_text(string)

# other imports    
# import dlipr # unused
import numpy as np
import random
import matplotlib.pyplot as plt
import os
from pathlib import Path
import tensorflow as tf
from tensorflow import keras
models = keras.models
layers = keras.layers

# make output directory ../data/results_taskX.Y_runZ
output_label = "test_taskX.Y_runZ"
output_path = Path.cwd().parent / "data" / output_label
output_path.mkdir(parents=True, exist_ok=True)
printcomet("output directory: {}".format(output_path))

printcomet("Constants: COMET_EXPERIMENT {}, NOTEBOOK {}".format(
    COMET_EXPERIMENT, NOTEBOOK))
printcomet("""
# ==============================================================================
# Data Preprocessing
# ==============================================================================
""")
printcomet("# load data")
(X_train, y_train), (X_test, y_test) = keras.datasets.mnist.load_data()
X_test_original = X_test.copy()
classes = np.arange(10) # numbers 0 to 9
    
printcomet("X_train shape {}".format(X_train.shape))
printcomet("y_train shape {}".format(y_train.shape))
printcomet("X_test shape {}".format(X_test.shape))
printcomet("y_test shape {}".format(y_test.shape))
printcomet("classes = categories: {}".format(list(classes)))
    
def plot_some_examples(dir : Path = Path.cwd()):
    filename = "examples.png"
    printcomet("plot {} using code from [1]".format(filename))
    # plot 3x3 grid of mnist number inputs
    plt.rcParams['figure.figsize'] = (9,9) # Make the figures a bit bigger

    for i in range(9):
        plt.subplot(3,3,i+1)
        num = random.randint(0, len(X_train))
        plt.imshow(X_train[num], cmap='gray', interpolation='none')
        plt.title("Class {}".format(y_train[num]))
        
    plt.tight_layout()
    plt.savefig(str(dir / filename), bbox_inches='tight')
    if COMET_EXPERIMENT:
        experiment.log_image(str(dir / filename))
    
    # # optional:
    # # print last random mnist number as value matrix
    # def matprint(mat, fmt="g"):
    #     col_maxes = [max([len(("{:"+fmt+"}").format(x)) for x in col]) for col in mat.T]
    #     for x in mat:
    #         for i, y in enumerate(x):
    #             printcomet(("{:"+str(col_maxes[i])+fmt+"}").format(y), end="  ")
    #         printcomet("")
    # matprint(X_train[num])

        
plot_some_examples(output_path)

printcomet("# reshape the image matrices to vectors")
# meaning:reshape 60,000 28 x 28 matrices into 60,000 784-length vectors.
X_train = X_train.reshape(-1, 28**2) # same as (60000, 784)
X_test = X_test.reshape(-1, 28**2)
    
printcomet("{} training samples".format(X_train.shape[0]))
printcomet("{} test samples".format(X_test.shape[0]))
printcomet("Training matrix shape {}".format(X_train.shape))
printcomet("Testing matrix shape {}".format(X_test.shape))

printcomet("normalize")
# meaning: convert integer RGB values (0-255) to float values (0-1)
X_train = X_train.astype('float32') / 255
X_test = X_test.astype('float32') / 255

printcomet("convert class labels to one-hot encodings")
Y_train = keras.utils.to_categorical(y_train, 10)
Y_test = keras.utils.to_categorical(y_test, 10)

# Note: Both dlipr.mnist and tf.keras.datasets.mnist 
# are identical after data preprocessing:
# ---------------------------------
# X_train shape (60000, 28, 28)
# y_train shape (60000,)
# X_test shape (10000, 28, 28)
# y_test shape (10000,)
# 60000 training samples
# 60000 test samples
# Training matrix shape (60000, 784)
# Testing matrix shape (60000, 784)
# ---------------------------------

printcomet("""
# ==============================================================================
# Define model
# ==============================================================================
""")
model = models.Sequential([
    layers.Dense(units=128, input_shape=(784,)),
    layers.ReLU(),
    layers.Dropout(rate=0.5),
    layers.Dense(units=10),
    layers.Softmax()])

model.summary(print_fn=printcomet)

model.compile(
    loss=keras.losses.CategoricalCrossentropy(),
    optimizer=keras.optimizers.Adam(learning_rate=1e-3),
    metrics=[keras.metrics.Accuracy()])

printcomet("""    
# ==============================================================================
# Train
# ==============================================================================
""")
results = model.fit(
    X_train, Y_train, # `x` and `y` data
    batch_size=100, # Number of samples per gradient update. 
    epochs=100, # An epoch is an iteration over the entire `x` and `y` data provided.  
    verbose=2, # 0 = silent, 1 = progress bar, 2 = one line per epoch. 
    validation_split=0.1,  # split off 10% of training data for validation
    callbacks=[ # callback usages: logging, save to disk, early stopping, ...
        keras.callbacks.CSVLogger(str(output_path / "history.csv"))] 
        # streams epoch results to a CSV file
    )

printcomet("""
# ==============================================================================
# Postprocessing
# ==============================================================================
""")

# predicted probabilities for the test set
Yp = model.predict(X_test) # shape (10'000, 10) ...
yp = np.argmax(Yp, axis=1) # ... so axis=-1 does the same

def plot_prediction(dir : Path = Path.cwd()):
    printcomet("# plot some predictions using code from [1]")
    
    # collect indices of y_test labels we predicted correctly / wrong
    correct = np.nonzero(yp == y_test)[0]
    wrong = np.nonzero(yp != y_test)[0]
    
    def plot_prediction_grid(filename, y_indices, nrows=3):
        ncols = nrows
        total = nrows * nrows
        plt.figure()
        for i, iy in enumerate(y_indices[:total]):
            plt.subplot(nrows, ncols, i+1)
            plt.imshow(X_test_original[iy], cmap='gray', interpolation='none')
            # plt.imshow(X_test[iy].reshape(28,28), cmap='gray', interpolation='none')
            plt.title("Predicted {}, Class {}".format(yp[iy], y_test[iy]))
        plt.tight_layout()
        plt.savefig(str(dir / filename), bbox_inches='tight')
        if COMET_EXPERIMENT:
            experiment.log_image(str(dir / filename))
        
    plot_prediction_grid("prediction_correct.png", correct)
    plot_prediction_grid("prediction_wrong.png", wrong)
        
plot_prediction(output_path)
    

def plot_confusion(dir : Path = Path.cwd()):
    if COMET_EXPERIMENT:
        printcomet("# log confusion matrix using comet's built-in cm; code from [2]")
        
        labels = [str(num) for num in classes]
        
        # this produces a comet error: no upload: no idea why.
        # experiment.log_confusion_matrix(y_true=y_test, y_predicted=yp, labels=labels)
        
        # so instead we gonna build the matrix here, then upload that. that works.
        # this 'build matrix' code taken directly from [2].
        n = len(classes)
        bins = np.linspace(-0.5, n - 0.5, n + 1)
        C = np.histogram2d(y_test, yp, bins=bins)[0]
        C = C / np.sum(C, axis=0) * 100
        experiment.log_confusion_matrix(matrix=C, labels=labels)
    else:
        print("# no comet experiment, so no confusion matrix")

plot_confusion(output_path)

# ============================
# TODO: forgot to log test_loss
# current understanding is
# not florian error = 1 - accuracy, but
# baptiste:
# val_error = abs(val_loss - train_loss) where comet Loss==train_loss. cmp. L2 p.21
# test_error = abs(test_loss - ..train_loss i think), 
# where we get test_loss from 
# test_loss, test_acc = model.evaluate(X_test, Y_test)
# but now in lecture4, 2 students who answered the chat question, said it's 
# florian's definition
# =============================

printcomet("""
# ==============================================================================
# End
# ==============================================================================
""")
if COMET_EXPERIMENT:
    experiment.end()
