﻿from comet_ml import Experiment
import dlipr
import numpy as np
from numpy import array
import random as rnd
import os
import tensorflow as tf
from tensorflow import keras

models = keras.models
layers = keras.layers


# Set up YOUR experiment - login to comet, create new project (for new exercise)
# and copy the statet command
# or just change the name of the workspace, and the API (you can find it in the settings)


# ----------------------------------------------
# Data
# ----------------------------------------------
# IF THE FOLLOWING LINE BREAKS - Check if you added already: software community/dlipr to your .profile as discussed in lecture 1
# after adding the line: login and logout WITHOUT restoring the old session!
data = dlipr.mnist.load_data()  

# plot some examples
#data.plot_examples(fname='examples.png')

# reshape the image matrices to vectors
X_train = data.train_images.reshape(-1, 28**2)
X_test = data.test_images.reshape(-1, 28**2)
print('%i training samples' % X_train.shape[0])
print('%i test samples' % X_test.shape[0])

# convert integer RGB values (0-255) to float values (0-1)
X_train = X_train.astype('float32') / 255
X_test = X_test.astype('float32') / 255

# convert class labels to one-hot encodings
Y_train = tf.keras.utils.to_categorical(data.train_labels, 10)
Y_test = tf.keras.utils.to_categorical(data.test_labels, 10)


# ----------------------------------------------
# Model and training
# ----------------------------------------------

# make output directory
folder = 'results/'

if not os.path.exists(folder):
    os.makedirs(folder)
    
# Initialization of varaibles for saving and comparing
N = 50              # number of iterations
parameters = []     # list to save the best hyperparameters
best = 0            # iteration with smalles validation lost (start at 0)
val_loss_best = 10  # saved lowest validation loss (initialized with high dummy value) 

# Loop to find the smallest validation loss
for i in range(N):
    
    # Generate random Hyperparameters
    neurons = 128 # kept constant as required 
    activfunc = rnd.choice(['relu', 'sigmoid', 'tanh', 'elu', 'selu'])
    learningrate = rnd.choice([0.0001, 0.001, 0.01, 0,1]) 
    dropout = rnd.choice([0.1, 0.2, 0.3, 0.4, 0.5]) 
    batchsize = rnd.choice([8, 16, 32, 64, 128, 256, 512])
    
    # build model
    model = models.Sequential([
        layers.Dense(neurons, input_shape=(784,)),
        layers.Activation(activfunc),
        layers.Dropout(dropout),
        layers.Dense(10),
        layers.Activation('softmax')])

    # compile model
    model.compile(
        loss='categorical_crossentropy',
        optimizer=keras.optimizers.Adam(lr=learningrate),
        metrics=['accuracy'])
    
    # train model
    results = model.fit(
        X_train, Y_train,
        batch_size=batchsize,
        epochs=10,
        verbose=2,
        validation_split=0.1)

    print("Current iteration is %s of %s" % (i, N))
    
    val_loss_list = results.history['val_loss']
    val_loss  =  val_loss_list[-1]
    # save new values and iteration if the validation loss is smaller than the previous
    if val_loss_best > val_loss:
        best = i
        val_loss_best = val_loss
        parameters = [neurons, activfunc, learningrate, dropout, batchsize]
    
    print("Best validation loss is %s at iteration %s" % (val_loss_best, best))
    val_loss_list = []
    
    #reinitialize the model
    keras.backend.clear_session()

# execute and save the experiment to comet with the best hyperparameter configuration
experiment = Experiment(api_key="<HIDDEN>",
                        project_name="exercise3_task2", workspace="irratzo")

print(parameters)
neurons = parameters[0]
activation_func = parameters[1]
dropout = parameters[3]
lr_rate = parameters[2]
batchsize = parameters[4]

for i in range(1):
    model = models.Sequential([
        layers.Dense(neurons, input_shape=(784,)),
        layers.Activation(activation_func),
        layers.Dropout(dropout),
        layers.Dense(10),
        layers.Activation('softmax')])
    print(model.summary())
    model.compile(
        loss='categorical_crossentropy',
        optimizer=keras.optimizers.Adam(lr=lr_rate),
        metrics=['accuracy'])
    results = model.fit(
        X_train, Y_train,
        batch_size=batchsize,
        epochs=10,
        verbose=2,
        validation_split=0.1,  # split off 10% of training data for validation
        callbacks=[keras.callbacks.CSVLogger(folder + 'history.csv')])
        
experiment.log_parameter("dropout", dropout)
experiment.log_parameter("activation", activation_func)
experiment.log_parameter("learningrate", lr_rate)
experiment.log_parameter("batchsize", batchsize)
experiment.log_parameter("epoch_best_fit", best)


# ----------------------------------------------
# Some plots
# ----------------------------------------------

# predicted probabilities for the test set
Yp = model.predict(X_test)
yp = np.argmax(Yp, axis=1)

# plot some test images along with the prediction
for i in range(20):

    dlipr.utils.plot_prediction(
        Yp[i],
        data.test_images[i],
        data.test_labels[i],
        data.classes,
        fname=folder + 'test-%i.png' % i)

# plot the confusion matrix
fig = dlipr.utils.plot_confusion(yp, data.test_labels, data.classes,
                                 fname=folder + 'confusion.png')
experiment.log_figure(figure=fig)



