from comet_ml import Experiment
import dlipr
import numpy as np
import os
import tensorflow as tf
from tensorflow import keras
models = keras.models
layers = keras.layers


# Set up YOUR experiment - login to comet, create new project (for new exercise)
# and copy the statet command
# or just change the name of the workspace, and the API (you can find it in the settings)
experiment = Experiment(api_key="EnterYourAPIKey",
                        project_name="exercise3", workspace="EnterGroupWorkspaceHere")

# ----------------------------------------------
# Data
# ----------------------------------------------
# IF THE FOLLOWING LINE BREAKS - Check if you added already: software community/dlipr to your .profile as discussed in lecture 1
# after adding the line: login and logout WITHOUT restoring the old session!
data = dlipr.mnist.load_data()  

# plot some examples
data.plot_examples(fname='examples.png')

# reshape the image matrices to vectors
X_train = data.train_images.reshape(-1, 28**2)
X_test = data.test_images.reshape(-1, 28**2)
print('%i training samples' % X_train.shape[0])
print('%i test samples' % X_test.shape[0])

# convert integer RGB values (0-255) to float values (0-1)
X_train = X_train.astype('float32') / 255
X_test = X_test.astype('float32') / 255

# convert class labels to one-hot encodings
Y_train = tf.keras.utils.to_categorical(data.train_labels, 10)
Y_test = tf.keras.utils.to_categorical(data.test_labels, 10)


# ----------------------------------------------
# Model and training
# ----------------------------------------------

# make output directory
folder = 'results/'

if not os.path.exists(folder):
    os.makedirs(folder)

model = models.Sequential([
    layers.Dense(128, input_shape=(784,)),
    layers.Activation('relu'),
    layers.Dropout(0.5),
    layers.Dense(10),
    layers.Activation('softmax')])

print(model.summary())

model.compile(
    loss='categorical_crossentropy',
    optimizer=keras.optimizers.Adam(lr=1e-3),
    metrics=['accuracy'])

results = model.fit(
    X_train, Y_train,
    batch_size=100,
    epochs=10,
    verbose=2,
    validation_split=0.1,  # split off 10% of training data for validation
    callbacks=[keras.callbacks.CSVLogger(folder + 'history.csv')])


# ----------------------------------------------
# Some plots
# ----------------------------------------------

# predicted probabilities for the test set
Yp = model.predict(X_test)
yp = np.argmax(Yp, axis=1)

# plot some test images along with the prediction
for i in range(20):

    dlipr.utils.plot_prediction(
        Yp[i],
        data.test_images[i],
        data.test_labels[i],
        data.classes,
        fname=folder + 'test-%i.png' % i)

# plot the confusion matrix
fig = dlipr.utils.plot_confusion(yp, data.test_labels, data.classes,
                                 fname=folder + 'confusion.png')
experiment.log_figure(figure=fig)
