# ==============================================================================
# exercise3 task1: Hyperparameter grid search for n.network for MNIST dataset
# ==============================================================================

# Usage:
# 1) select two hyperparameters in variable hyp_search_selection
# 2) look up the respective hyp. indices i,j,k,l,m in var. hyp of selection
# 3) adjust indices in all mentions of 'ADJUST LOOP INDICES TO SELECTION'


# ==============================================================================
# Constants, Imports
# ==============================================================================
# constants
COMET_EXPERIMENT = True
NOTEBOOK = False
REFERENCES = [
    "https://github.com/AviatorMoser/keras-mnist-tutorial", # [1]
    "https://github.com/DavidWalz/dlipr/blob/master/dlipr/utils.py", # [2]
    ]

# setup comet
# for notebook (eg google colab). only needed if comet_ml not installed yet.
if COMET_EXPERIMENT and NOTEBOOK:
    pass # if notebook, uncomment:
    # %pip install comet_ml
# after installation for first time, restart notebook!

if COMET_EXPERIMENT:
    # import comet_ml in the top of your file    
    from comet_ml import Experiment
  
def printcomet(string : str = ""):
    # append references (like for "... [1] ..." and so on)
    reflabl = ["[{}]".format(i+1) for i in range(len(REFERENCES))]
    if any([labl in string for labl in reflabl]):
      string += "\nReferences:"
      for i,labl in enumerate(reflabl):
        if labl in string:
          string += "\n{} {}".format(labl, REFERENCES[i])
    
    # print and log
    print(string)
    if COMET_EXPERIMENT:
        experiment.log_text(string)

# other imports  
import dlipr
import numpy as np
from numpy import array
import matplotlib.pyplot as plt
import random as rnd
import os
import tensorflow as tf
from tensorflow import keras
models = keras.models
layers = keras.layers


# Set up YOUR experiment - login to comet, create new project (for new exercise)
# and copy the statet command
# or just change the name of the workspace, and the API (you can find it in the settings)


# ----------------------------------------------
# Data
# ----------------------------------------------
# IF THE FOLLOWING LINE BREAKS - Check if you added already: software community/dlipr to your .profile as discussed in lecture 1
# after adding the line: login and logout WITHOUT restoring the old session!
data = dlipr.mnist.load_data()
# to be consistent with common keras.datasets.mnist notation:
X_train = data.train_images
X_test = data.test_images
y_train = data.train_labels
y_test = data.test_labels
classes = np.arange(10) # numbers 0 to 9
X_test_original = X_test.copy()

# plot some examples
#data.plot_examples(fname='examples.png')

# reshape the image matrices to vectors
X_train = data.train_images.reshape(-1, 28**2)
X_test = data.test_images.reshape(-1, 28**2)
print('%i training samples' % X_train.shape[0])
print('%i test samples' % X_test.shape[0])

# convert integer RGB values (0-255) to float values (0-1)
X_train = X_train.astype('float32') / 255
X_test = X_test.astype('float32') / 255

# convert class labels to one-hot encodings
Y_train = tf.keras.utils.to_categorical(data.train_labels, 10)
Y_test = tf.keras.utils.to_categorical(data.test_labels, 10)


# ==============================================================================
# Grid search
# ==============================================================================


# ----------------------------------------------
# Model and training
# ----------------------------------------------

# make output directory
folder = 'results/'

if not os.path.exists(folder):
    os.makedirs(folder)
    
# # for testing: smaller grid
# # hyperparameter values values for grid search
# hyp = {
#     "neurons" : [64,128,256], # i
#     "activfunc" : ['relu', 'sigmoid', 'tanh', 'elu', 'selu'], # j
#     "dropout" : [0.1, 0.3, 0.5], # k
#     "learningrate" : [1e-4, 1e-3, 1e-2, 1e-1], # l
#     "batchsize" : [8, 16, 32, 64, 128, 256, 512], # m
# }
# # standard selection indices
# (i, j, k, l, m) = (1, 0, 2, 1, 4)
    
# for production run:
# hyperparameter values values for grid search
hyp = {
    "neurons" : [16, 32, 64, 128, 256, 512], # i
    "activfunc" : ['relu', 'sigmoid', 'tanh', 'elu', 'selu'], # j
    "dropout" : [0.1, 0.2, 0.3, 0.4, 0.5], # k
    "learningrate" : [1e-4, 1e-3, 1e-2, 1e-1], # l
    "batchsize" : [8, 16, 32, 64, 128, 256, 512], # m
}
# standard selection indices
(i, j, k, l, m) = (3, 0, 4, 1, 4)

# --> THIS IS THE SELECTION! <--
hyp_search_selection = ["neurons", "dropout"] # SELECTION INDICES: (i, k)
hss = hyp_search_selection

# init grids for results
metrics = ["loss", "val_loss", "accuracy", "val_accuracy"]
grid_shape = tuple([len(hyp[hyp_sel]) for hyp_sel in hss])
metrics_grids = {metric : np.zeros(grid_shape) for metric in metrics}

# Loop to find the smallest validation loss
# --> ADJUST LOOP INDICES TO SELECTION! <--
print("# starting gridsearch")
for i in range(grid_shape[0]):
    for k in range(grid_shape[1]):
        
        # build model
        model = models.Sequential([
            layers.Dense(hyp["neurons"][i], input_shape=(784,)),
            layers.Activation(hyp["activfunc"][j]),
            layers.Dropout(hyp["dropout"][k]),
            layers.Dense(10),
            layers.Activation('softmax')])
    
        # compile model
        model.compile(
            loss='categorical_crossentropy',
            optimizer=keras.optimizers.Adam(lr=hyp["learningrate"][l]),
            metrics=['accuracy'])
        
        # train model
        results = model.fit(
            X_train, Y_train,
            batch_size=hyp["batchsize"][m],
            epochs=10,
            verbose=2,
            validation_split=0.1)
    
        print("gridsearch iter {}_({},{}) = ({}, {})".format(
            hss, i, k, hyp[hss[0]][i], hyp[hss[1]][k]))
            # --> ADJUST LOOP INDICES TO SELECTION! (here: TWO times!) <--
        
        # ref for accessing these: https://stackoverflow.com/a/50137577/8116031,
        # and printing results.history.keys().
        for metric in metrics:
            metrics_grids[metric][i,k] = results.history[metric][-1]
            # --> ADJUST LOOP INDICES TO SELECTION! <--
        
        #reinitialize the model
        keras.backend.clear_session()

for metric in metrics:
    print("grid search result for metric {}:".format(metric))
    print(metrics_grids[metric])
    
print("# gridsearch ended, start comet experiment")


# ==============================================================================
# Comet experiment with grid search result
# ==============================================================================


# execute and save the experiment to comet with the best hyperparameter configuration
if COMET_EXPERIMENT:
    experiment = Experiment(api_key="E7YQLfWBuQZXjw0c2dKsXKPsY",
                            project_name="exercise3", workspace="irratzo")

# produce plots of the grid search metrics                           
# reference: https://stackoverflow.com/a/50275637/8116031
for metric in metrics:
    plt.matshow(metrics_grids[metric]);
    plt.colorbar()
    plt.xlabel(hss[0])
    plt.ylabel(hss[1])
    plt.xticks(np.arange(grid_shape[0]), hyp[hss[0]])
    plt.yticks(np.arange(grid_shape[1]), hyp[hss[1]])
    plt.title(metric)
    plt.savefig(folder + metric + ".png", bbox_inches='tight')
    if COMET_EXPERIMENT:
        experiment.log_image(folder + metric + ".png")

# determine the best combi and use that to log a comet experiment,
# using "val_loss" as indicator, like in random search
# --> ADJUST LOOP INDICES TO SELECTION! <--
(i, k) = np.unravel_index(metrics_grids["val_loss"].argmin(), grid_shape)

printcomet("# performed offline gridsearch on hyp.param.s {}".format(hyp_search_selection))
printcomet("# on grid:\n{}".format(hyp))
printcomet("# chose best performance indicator: val_loss, to run comet experiment")
printcomet("# this best-performing selection has the used hyp.params logged")
printcomet("# the best-performing sel. indices are indices of {} = ({},{})".format(
    hss, i, k))
# --> ADJUST LOOP INDICES TO SELECTION! <--

# build model
model = models.Sequential([
    layers.Dense(hyp["neurons"][i], input_shape=(784,)),
    layers.Activation(hyp["activfunc"][j]),
    layers.Dropout(hyp["dropout"][k]),
    layers.Dense(10),
    layers.Activation('softmax')])
model.summary(print_fn=printcomet)    

# compile model
model.compile(
    loss='categorical_crossentropy',
    optimizer=keras.optimizers.Adam(lr=hyp["learningrate"][l]),
    metrics=['accuracy'])

# train model
results = model.fit(
    X_train, Y_train,
    batch_size=hyp["batchsize"][m],
    epochs=10,
    verbose=2,
    validation_split=0.1)
 
if COMET_EXPERIMENT:  
    experiment.log_parameter("neurons", hyp["neurons"][i])
    experiment.log_parameter("activation", hyp["activfunc"][j])
    experiment.log_parameter("dropout", hyp["dropout"][k])
    experiment.log_parameter("learningrate", hyp["learningrate"][l])
    experiment.log_parameter("batchsize", hyp["batchsize"][m])


# ----------------------------------------------
# Some plots
# ----------------------------------------------

# predicted probabilities for the test set
Yp = model.predict(X_test)
yp = np.argmax(Yp, axis=1)

# plot some test images along with the prediction
for i in range(20):

    dlipr.utils.plot_prediction(
        Yp[i],
        data.test_images[i],
        data.test_labels[i],
        data.classes,
        fname=folder + 'test-%i.png' % i)
    if COMET_EXPERIMENT:
        experiment.log_image(folder + 'test-%i.png' % i)

# # plot the confusion matrix
# fig = dlipr.utils.plot_confusion(yp, data.test_labels, data.classes,
#                                  fname=folder + 'confusion.png')

def plot_confusion():
    if COMET_EXPERIMENT:
        printcomet("# log confusion matrix using comet's built-in cm; code from [2]")
        
        labels = [str(num) for num in classes]
        
        # this produces a comet error: no upload: no idea why.
        # experiment.log_confusion_matrix(y_true=y_test, y_predicted=yp, labels=labels)
        
        # so instead we gonna build the matrix here, then upload that. that works.
        # this 'build matrix' code taken directly from [2].
        n = len(classes)
        bins = np.linspace(-0.5, n - 0.5, n + 1)
        C = np.histogram2d(y_test, yp, bins=bins)[0]
        C = C / np.sum(C, axis=0) * 100
        experiment.log_confusion_matrix(matrix=C, labels=labels)
    else:
        print("# no comet experiment, so no confusion matrix")

plot_confusion()

# ============================
# TODO: forgot to log test_loss
# current understanding is
# not florian error = 1 - accuracy, but
# baptiste:
# val_error = abs(val_loss - train_loss) where comet Loss==train_loss. cmp. L2 p.21
# test_error = abs(test_loss - ..train_loss i think), 
# where we get test_loss from 
# test_loss, test_acc = model.evaluate(X_test, Y_test)
# but now in lecture4, 2 students who answered the chat question, said it's 
# florian's definition
# =============================
if COMET_EXPERIMENT:
    experiment.end()
