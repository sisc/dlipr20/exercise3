#Correction
##Task 0
Very nice discussion :)
0/0 P
##Task 1
###Task 1.1
3/3 P
###Task 1.2
2/2 P
###Task 1.3
1/1 P
###Task 1.4
2/2 P
##Task 2
###Task 2.1
3/3 P
###Task 2.2
How do you define the point, where a good combination of hyperparameters is found? It seems to me, that there is a threshold at an loss of ~0.075 or something similar. But there are two outlier or is this a problem of the plot?

Nevertheless this is not really what you were supposed to do. Also the standard deviation as a function of number of trials is not displayed, what was requiered.

The other discussions are well done. The learning rate plot has a bad binning.

3/4 P
###Task 2.3
1/1 P
###Task 2.4
1/2 P
#Total 16/18 P Very much effort :)


**RWTH Aachen - Deep Learning in Physics Research SS2020**

**Exercise 3 Optimization and Hyperparameter Tuning: MNIST**

Date: 10.05.2020

Students:
| student            | rwth ID |
|--------------------|---------|
| Baptiste Corneglio | 411835|
| Florian Stadtmann  | 367319 |
| Johannes Wasmer    |  090800 |



**Table of Contents**

- [Preliminary remarks](#preliminary-remarks)
    - [Task2 is in another project](#task2-is-in-another-project)
    - [Experiment tagging](#experiment-tagging)
    - [Tuning libraries](#tuning-libraries)
- [Task0: Modify different hyperparameters manually and observe their influence on the model performance. [ungraded]](#task0-modify-different-hyperparameters-manually-and-observe-their-influence-on-the-model-performance-ungraded)
    - [Hyperparameters in general](#hyperparameters-in-general)
    - [Hyperparameters considered in this exercise](#hyperparameters-considered-in-this-exercise)
    - [Hyperparameters considered by comet.ml](#hyperparameters-considered-by-cometml)
    - [Manual tuning](#manual-tuning)
- [Task1: Select two hyperparameters of your choice and perform grid searches in appropriate intervals. [8 P]](#task1-select-two-hyperparameters-of-your-choice-and-perform-grid-searches-in-appropriate-intervals-8-p)
    - [task1.1. Submit the relevant parts of your code, with modifications highlighted. [3 P]](#task11-submit-the-relevant-parts-of-your-code-with-modifications-highlighted-3-p)
        - [grid search](#grid-search)
            - [define hyperparameter values sets](#define-hyperparameter-values-sets)
            - [2d-loop over these sets](#2d-loop-over-these-sets)
            - [identify hyp. value combi. with best metric (validation error)](#identify-hyp-value-combi-with-best-metric-validation-error)
        - [run comet experiment with this one model](#run-comet-experiment-with-this-one-model)
            - [produce plots](#produce-plots)
    - [task1.2. Plot or state the validation loss for all combinations of hyperparameters. [2 P]](#task12-plot-or-state-the-validation-loss-for-all-combinations-of-hyperparameters-2-p)
        - [gridsearch `neurons--dropout`](#gridsearch-neurons--dropout)
        - [gridsearch `neurons--learningrate`](#gridsearch-neurons--learningrate)
        - [gridsearch `dropout--learningrate`](#gridsearch-dropout--learningrate)
        - [Discussion](#discussion)
    - [task1.3. State the best values for hyperparameters in the chosen interval. [1 P]](#task13-state-the-best-values-for-hyperparameters-in-the-chosen-interval-1-p)
        - [gridsearch `neurons--dropout`](#gridsearch-neurons--dropout-1)
        - [gridsearch `neurons--learningrate`](#gridsearch-neurons--learningrate-1)
        - [gridsearch `dropout--learningrate`](#gridsearch-dropout--learningrate-1)
        - [Discussion](#discussion-1)
    - [task1.4. Give the validation and test error for these best values. [2 P]](#task14-give-the-validation-and-test-error-for-these-best-values-2-p)
        - [Discussion](#discussion-2)
- [Task2: Perform random sampling with four hyperparameters: [10 P]](#task2-perform-random-sampling-with-four-hyperparameters-10-p)
    - [task2.1. Submit the relevant parts of your code, with modifications highlighted. [3 P]](#task21-submit-the-relevant-parts-of-your-code-with-modifications-highlighted-3-p)
    - [task2.2. How many trials are necessary to find a good combination of hyperparameters? [4 P]](#task22-how-many-trials-are-necessary-to-find-a-good-combination-of-hyperparameters-4-p)
    - [task2.3. State the best values for hyperparameters. [1 P]](#task23-state-the-best-values-for-hyperparameters-1-p)
    - [task2.4. Give the validation and test error for these best values. [2 P]](#task24-give-the-validation-and-test-error-for-these-best-values-2-p)
- [References](#references)



# Preliminary remarks #

## Task2 is in another project ##

The experiments for exercise3 task2 are in the project [exercise3-task2](https://www.comet.ml/irratzo/exercise3-task2). The notes for task0, task1 and task2 are here.

## Experiment tagging ##

Final experiments in this project (task0 and task1) are tagged with the tag `final`, the respective task tag, e.g. `taskX.Y`, and optionally more tags to better distinguish them. Experiments tagged with `dev`, should all be hidden away in the [archive](https://www.comet.ml/irratzo/exercise3/archive). They are development stage and **not** final. Specific experiments are referred to in the notes via their tags and/or hash or hyperlink.





## Tuning libraries ##

Since hyp.opt. = hyperparameter optimization = hyperparameter tuning (= search, ...) is such an integral but labor-intensive part of deep learning, there are many libraries trying to alleviate this. `scikit-learn` supports grid search, random search and bayesian optimization for models defined with `keras` [3] [4] [5]. And the newer libraries `keras tuner` and `comet_ml` offer much more advanced tuning algorithms [6] [7]. Wikipedia gives another overview of such hyperparameter otpimization libraries [8].

However, here we make use of none of these tuning libraries. Instead we try to
implement grid and random search on our own.

# Task0: Modify different hyperparameters manually and observe their influence on the model performance. [ungraded] #

This task corresponds to manual hyperparameter tuning. This approach is (jokingly) also called 'grad student decent' [5], or more aptly by Andrew Ng, 'Panda care' [2].

## Hyperparameters in general ##

Reference [5] distinguishes three general neural network parameter categories [5]:

1) model design parameters
  - network depth
  - weight initialization
  - per layer:
    - layer width = no. of units
    - choice of activation function
    - dropout yes/no
  - choice of optimizer
  - choice of loss

2) hyperparameters
  - do := dropout rate(s)
  - lr := learning rate
  - bs := batch size

3) model paramaters (learned through training/model fit)
  - weights &amp; biases

**However, the lecture [1]** (and most other references), categorize **all*** network parameters which are *not learned* as hyperparameters. Here's the, also non-exhaustive, list from the lecture [1]:

- `hyp_ilr`:= initial learning rate: typical 1e-3. too small: too many steps to reach the minimum. too large: oscillations around the minimum.
- `hyp_lrs`:= learning rate schedule. see slides [1] p.21.
- `hyp_noe`:= number of epochs = number of iterations. stop when loss reduction stops.
- `hyp_mbs`:= mini-batch size. Stochastic Gradient Optimizers (relevant ones in
  `tf.keras.optimizers` are `SGD`, `Adagrad`, `RMSProp`, `Adam`) use randomly
  selected subsets of the training set, minibatches, to estimate the expectation
  value of the gradient. small: more updates per epoch, more regularization. large: less time per epoch, less stochasticity.
- `hyp_nou` := number of units (per layer) = number of neurons = hidden layer widths. too small: no loss reduction. too large: takes too long.
- `hyp_rc` := regularization coefficients (per layer):
  - `hyp_np` := norm penalty
  - `hyp_dor` := dropout rate / fraction. For best-practice keras settings from
    original paper, see [9].
- `hyp_af` := activation functions (per layer).n
- `hyp_ib` := initial biases. set to zero.
- `hyp_iw` := initial weights. see slides [1] p.23.

Along with these come dos and don'ts:

- split data into training, validation, test set.
- for hyp.opt. more than three values per hyp.
- if best value on border, extend search.
- use appropriate value scale for a hyp. (e.g. log for `h_ilr**).
- turn off early stopping during hyp.opt.
- grid search: for 1-3 hyp.
- random sampling: for 2-5 hyp. Sample values from prior distribution.
- for 3-more hyp., maybe try advanced search algorithms like e.g. bayesian.

## Hyperparameters considered in this exercise ##

**For this exercise3, a neural network for the classification of the MNIST dataset was given. The network provided as-is has one hidden layer, and it has these 6 hyperparameters to tune:**

- for the hidden layer: `hyp_nou=128`, `hyp_af=ReLU`, `hyp_dor=0.3`
- for the optimizer: `hyp_ilr=1e-3`
- for the training phase: `hyp_mbs=100`, `hyp_noe=10`

We will discount `hyp_noe` for tuning and just set it to a large enough number. **So are left with 5 hyperparameters to tune for this model.**

**From now on we will refer to these remaining hyperparameters as** `neurons`, `activation`, `dropout`, `learningrate`, `batchsize`, **and** `epochs`.

It should be noted that the network *depth* is another hyp. that we completely ignore in this exercise, despite its great importance.

## Hyperparameters considered by comet.ml ##

For this exercise3, the hyperparameters logged by comet for running the provided network as-is, with only our remaining hyp.s added via `experiment.log_parameter()`:

```text
activation	relu
Adam_amsgrad	false
Adam_beta_1	0.9
Adam_beta_2	0.999
Adam_decay	0.0
Adam_epsilon	1.0E-7
Adam_learning_rate	0.001
Adam_name	Adam
batchsize	128
batch_size	128
curr_epoch	9
curr_step	4690
dropout	0.4
epochs	10
learningrate	0.001
neurons	256
Optimizer	Adam
samples	54000
steps	422
validate_curr_step	4684
```

The overlap with the hyp.s considered by us is the set `learningrate`, `batchsize`, `epochs` (discounted). `neurons`, `activation`, `dropout`, though logged manually here by us, are by not considered by comet to be hyperparameters, or are simply not logged by default as such.

## Manual tuning ##





In `model.fit() ` we set `epochs=100`.

This is about intuition. Trying three values per hyp. would already require 3^5=243 experiments which would defy the purpose.

We would expect that `neurons` is the most important hyp. Since the input images are almost black and white, we expect `activation` to always work best with ReLU, so we discount it for tuning. Intuition fails as to the relative merit between tuning `dropout`, `learningrate`, and `batchsize`.

Doubling `neurons=256` doubles the both `val_acc` and `acc`, while not affecting `val_batch_loss` and `batch_loss`.

Tenfolding `learningrate=1e-2` roughly quadruples both `val_acc` and `acc`, while roughly doubling  `val_batch_loss` and `batch_loss`.

Halving `dropout=0.2` slightly increases both `val_acc` and `acc`, while not affecting `val_batch_loss` and decreasing `batch_loss`.

Tenfolding `batchsize=1000` (and tenfolding `epochs=1000` so no. of steps remains the same ca. 60k), roughly quadruples both `val_acc` and `acc`, while


# Task1: Select two hyperparameters of your choice and perform grid searches in appropriate intervals. [8 P] #

## task1.1. Submit the relevant parts of your code, with modifications highlighted. [3 P] ##

The bird's eye view of our grid search code is this.

``` python
load, prepare and split data into training, validation, test sets

grid search:
define hyperparameter values sets
2d-loop over these sets:
  define model
  compile model
  train model
  2d-save metrics

identify hyp. value combi. with best metric (validation error)

run comet experiment with this one model:
  define model
  compile model
  train model
  test model
  produce plots
  log everything
```

Our code is hard-coded for 2d-grid searches. However, the choice of these two
hyperparameters can be easily switched via index change.

Now the real code from the sections above. The selection hear is on the hyp.s
`neurons` and `dropout`, respectively hyp. value indices `(i,k)`. Some code
sections shifted around unchanged, for better logical readability. All results
for `task1` were run with this code.

### grid search ###

#### define hyperparameter values sets ####

The same value sets were chosen as for task2 random search for comparability.

``` python
# hyperparameter values values for grid search
hyp = {
    &quot;neurons&quot; : [16, 32, 64, 128, 256, 512], # i
    &quot;activfunc&quot; : ['relu', 'sigmoid', 'tanh', 'elu', 'selu'], # j
    &quot;dropout&quot; : [0.1, 0.2, 0.3, 0.4, 0.5], # k
    &quot;learningrate&quot; : [1e-4, 1e-3, 1e-2, 1e-1], # l
    &quot;batchsize&quot; : [8, 16, 32, 64, 128, 256, 512], # m
}
# standard selection indices
(i, j, k, l, m) = (3, 0, 4, 1, 4)

hyp_search_selection = [&quot;neurons&quot;, &quot;dropout&quot;] # SELECTION INDICES: (i, k)
```

#### 2d-loop over these sets ####

``` python
# init grids for results
metrics = [&quot;loss&quot;, &quot;val_loss&quot;, &quot;accuracy&quot;, &quot;val_accuracy&quot;]
grid_shape = tuple([len(hyp[hyp_sel]) for hyp_sel in hss])
metrics_grids = {metric : np.zeros(grid_shape) for metric in metrics}

# Loop to find the smallest validation loss
# --&gt; ADJUST LOOP INDICES TO SELECTION! &lt;--
print(&quot;# starting gridsearch&quot;)
for i in range(grid_shape[0]):
    for k in range(grid_shape[1]):

        # build model
        model = models.Sequential([
            layers.Dense(hyp[&quot;neurons&quot;][i], input_shape=(784,)),
            layers.Activation(hyp[&quot;activfunc&quot;][j]),
            layers.Dropout(hyp[&quot;dropout&quot;][k]),
            layers.Dense(10),
            layers.Activation('softmax')])

        # compile model
        model.compile(
            loss='categorical_crossentropy',
            optimizer=keras.optimizers.Adam(lr=hyp[&quot;learningrate&quot;][l]),
            metrics=['accuracy'])

        # train model
        results = model.fit(
            X_train, Y_train,
            batch_size=hyp[&quot;batchsize&quot;][m],
            epochs=10,
            verbose=2,
            validation_split=0.1)

        print(&quot;gridsearch iter {}_({},{}) = ({}, {})&quot;.format(
            hss, i, k, hyp[hss[0]][i], hyp[hss[1]][k]))
            # --&gt; ADJUST LOOP INDICES TO SELECTION! (here: TWO times!) &lt;--

        # ref for accessing these: https://stackoverflow.com/a/50137577/8116031,
        # and printing results.history.keys().
        for metric in metrics:
            metrics_grids[metric][i,k] = results.history[metric][-1]
            # --&gt; ADJUST LOOP INDICES TO SELECTION! &lt;--

        #reinitialize the model
        keras.backend.clear_session()
```

#### identify hyp. value combi. with best metric (validation error) ####

``` python
# determine the best combi and use that to log a comet experiment,
# using &quot;val_loss&quot; as indicator, like in random search
# --&gt; ADJUST LOOP INDICES TO SELECTION! &lt;--
(i, k) = np.unravel_index(metrics_grids[&quot;val_loss&quot;].argmin(), grid_shape)
```

### run comet experiment with this one model ###

the exact same `model-compile-fit` code as in the loop above, just without the loop, and with logging.

#### produce plots ####

for the test results, the given plotting method was used.

the confusion matrix was done with comet instead of with `dlipr` (note:
references in code are separate references from here, and resolved in output).

``` python
def plot_confusion():
    if COMET_EXPERIMENT:
        printcomet(&quot;# log confusion matrix using comet's built-in cm; code from [2]&quot;)

        labels = [str(num) for num in classes]

        # this produces a comet error: no upload: no idea why.
        # experiment.log_confusion_matrix(y_true=y_test, y_predicted=yp, labels=labels)

        # so instead we gonna build the matrix here, then upload that. that works.
        # this 'build matrix' code taken directly from [2].
        n = len(classes)
        bins = np.linspace(-0.5, n - 0.5, n + 1)
        C = np.histogram2d(y_test, yp, bins=bins)[0]
        C = C / np.sum(C, axis=0) * 100
        experiment.log_confusion_matrix(matrix=C, labels=labels)
    else:
        print(&quot;# no comet experiment, so no confusion matrix&quot;)

plot_confusion()
```

## task1.2. Plot or state the validation loss for all combinations of hyperparameters. [2 P] ##

Grid search was performed for three 2d hyp. selections:

  * `neurons--dropout`, experiment [3e03338d5](https://www.comet.ml/irratzo/exercise3/3e03338d510f4f479665669763b3b3ca)
  * `neurons--learningrate`, experiment [5d03bf607](https://www.comet.ml/irratzo/exercise3/5d03bf6079cd415daf14f5b285c818ff)
  * `dropout--learningrate`, experiment [748f6259e](https://www.comet.ml/irratzo/exercise3/748f6259e8484b009853e82234527a52)

We had 6, 5, and 4 values for these three hyperparameters. So for the grid
search, we performed (6 x 5) + (6 x 4) + (5 x 4) = 74 training runs. The curse
of dimensionality should already come apparent here: performing a 3d-gridsearch
for all three hyperparameters would require (6 x 5 x 4) = 120 training runs for
this small value set. The single best result would be much more useful, but the
distribution much harder to visualize.

We produced plots for the four metrics `model.fit` metrics validation loss, validation accuracy, loss, accuracy.

### gridsearch `neurons--dropout` ###

validation loss

![](https://www.comet.ml/api/image/notes/download?imageId=xSZg25VFDHPJObZTE3GLeYT5d&amp;objectId=02fc3416b6ef4c529d92b448932d7b1e)

validation accuracy

![](https://www.comet.ml/api/image/notes/download?imageId=FcRJQhJ2598Ao0EAawDpEQKQB&amp;objectId=02fc3416b6ef4c529d92b448932d7b1e)

loss

![](https://www.comet.ml/api/image/notes/download?imageId=8Nd4VKVnYvlfX0qwRGKk2w0EF&amp;objectId=02fc3416b6ef4c529d92b448932d7b1e)

accuracy

![](https://www.comet.ml/api/image/notes/download?imageId=iEDiO73srFv3AvMuBszDzm0Lz&amp;objectId=02fc3416b6ef4c529d92b448932d7b1e)


### gridsearch `neurons--learningrate` ###

validation loss

![](https://www.comet.ml/api/image/notes/download?imageId=bzlYf3zQohDqZTZ3pOmkW69Iz&amp;objectId=02fc3416b6ef4c529d92b448932d7b1e)

validation accuracy

![](https://www.comet.ml/api/image/notes/download?imageId=5AJm5dcj38jq62vM0jgnJS53f&amp;objectId=02fc3416b6ef4c529d92b448932d7b1e)

loss

![](https://www.comet.ml/api/image/notes/download?imageId=d9xighPwtt7rAX2GRE1oa8TUP&amp;objectId=02fc3416b6ef4c529d92b448932d7b1e)

accuracy

![](https://www.comet.ml/api/image/notes/download?imageId=RXQu7xQysbGyMdKPh5bOP8Z9R&amp;objectId=02fc3416b6ef4c529d92b448932d7b1e)


### gridsearch `dropout--learningrate` ###

validation loss

![](https://www.comet.ml/api/image/notes/download?imageId=Hrorqc8NfB9lxr1bygeMBUK2p&amp;objectId=02fc3416b6ef4c529d92b448932d7b1e)

validation accuracy

![](https://www.comet.ml/api/image/notes/download?imageId=z2eT2rhZW1rEMM1t9daAXIgDl&amp;objectId=02fc3416b6ef4c529d92b448932d7b1e)

loss

![](https://www.comet.ml/api/image/notes/download?imageId=n2diSIffrGk8p3K4wNNEWYKBM&amp;objectId=02fc3416b6ef4c529d92b448932d7b1e)

accuracy

![](https://www.comet.ml/api/image/notes/download?imageId=PGPQgrAHbCjUepaKUnZnWFkNi&amp;objectId=02fc3416b6ef4c529d92b448932d7b1e)

### Discussion ###

Note: some plots seem to have a bug, a mismatch between axis ticks (axis labeling) and the plot. This could be due to an error of the used method `plt.matshow` that is explained [here](https://stackoverflow.com/a/3532408/8116031). The matter could not be resolved. However, the values for the **best** fits in task1.3 and task1.4 are reasonable.

For `neurons--dropout`, it is striking that `val_acc` is better for more `neurons`, and `test_acc` better for *less* neurons.

For `neurons--learningrate` we see a sharp drop-off boundary of loss between 64 and 128 `neurons`, with *less* `neurons` faring better. Since the *numerical* best result (see task1.3 below) is  `neurons=512`, `learningrate=1e-3`, this is probably a plot glitch.

The same applies to `dropout-learningrate`, where the drop-off happens between `dropout` 0.3 and 0.4. However, here the numerical result (task 1.3.) `dropout=0.1`, `learningrate=1e-3` at least fits the plot.

In any case, it can be seen that the hyperparameter optimization landscape seems to have smooth gradients as well as sharp non-linearities.

## task1.3. State the best values for hyperparameters in the chosen interval. [1 P] ##

``` python
# hyperparameter values values for grid search
hyp = {
    &quot;neurons&quot; : [16, 32, 64, 128, 256, 512], # i
    &quot;activfunc&quot; : ['relu', 'sigmoid', 'tanh', 'elu', 'selu'], # j
    &quot;dropout&quot; : [0.1, 0.2, 0.3, 0.4, 0.5], # k
    &quot;learningrate&quot; : [1e-4, 1e-3, 1e-2, 1e-1], # l
    &quot;batchsize&quot; : [8, 16, 32, 64, 128, 256, 512], # m
}
# standard selection indices
(i, j, k, l, m) = (3, 0, 4, 1, 4)

```

Best values can be found in experiment &gt; output.

### gridsearch `neurons--dropout` ###

`# the best-performing sel. indices are indices of ['neurons', 'dropout'] = (4,3)`

This is `neurons=256`, `dropout=0.4`.

### gridsearch `neurons--learningrate` ###

`# the best-performing sel. indices are indices of ['neurons', 'learningrate'] = (5,1)`

This is `neurons=512`, `learningrate=1e-3`.

### gridsearch `dropout--learningrate` ###

`# the best-performing sel. indices are indices of ['dropout', 'learningrate'] = (0,1)`

This is `dropout=0.1`, `learningrate=1e-3`.

### Discussion ###

The results show that the 2d search is not stable since e.g. `dropout` changes
drastically. However, the values for `neurons` and `learningrate` confirm what
we found in task0.

## task1.4. Give the validation and test error for these best values. [2 P] ##

Here, we give the validation and test error for the best respective
hyperparameter value combination not from the grid search above, but from the
comet experiment that was run and logged subsequently for this best-performing
combination, in terms of validation error. The reason is that at first, the
exact values from gridsearch were only plotted not logged. Now however, they are
logged as well.

We calculate the validation error as `val_err = abs(val_loss - Loss)`, where comet `Loss` = `training_loss`.

  * `neurons--dropout`, orange, experiment [3e03338d5](https://www.comet.ml/irratzo/exercise3/3e03338d510f4f479665669763b3b3ca): `val_loss` 0.0649, `loss` 0.0589, `val_error` = 0.006
  * `neurons--learningrate`, blue, experiment [5d03bf607](https://www.comet.ml/irratzo/exercise3/5d03bf6079cd415daf14f5b285c818ff): `val_loss` 0.0582, `loss` 0.0503, `val_error` = 0.0079
  * `dropout--learningrate`, green, experiment [748f6259e](https://www.comet.ml/irratzo/exercise3/748f6259e8484b009853e82234527a52): `val_loss` 0.0728 , `loss` 0.0481, `val_error` = 0.0247

We forgot to log the `test_loss` via `(test_loss, test_acc) = model.evaluate(X_test, Y_test)`.

validation loss

![](https://www.comet.ml/api/image/notes/download?imageId=5NvEqUqv63TotlIVscmeXzbWB&amp;objectId=02fc3416b6ef4c529d92b448932d7b1e =900x450)

test loss

![](https://www.comet.ml/api/image/notes/download?imageId=0oYdTIMqjBWNRSdPbl9sN5k9g&amp;objectId=02fc3416b6ef4c529d92b448932d7b1e =900x450)

### Discussion ###

The results show that the best results at least all converge. We only trained
for 10 epochs each through all runs.

# Task2: Perform random sampling with four hyperparameters: [10 P] #

## task2.1. Submit the relevant parts of your code, with modifications highlighted. [3 P] ##

First, we initialize the following variables:
- N: the number of times we run the model with randomnized hyperparameters
- parameters: list to save the best combination of hyperparameters
- best: number of the iteration at which the validation loss is lowest
- val_loss_best: variable for the lowest validation loss

```python
# Initilization of varaibles for saving and comparing
N = 50              # number of iterations
parameters = []     # list to save the best hyperparameters
best = 0            # iteration with smalles validation lost (start at 0)
val_loss_best = 10  # saved lowest validation loss (initialized with high dummy value)
```

We know loop N times to find the best hyperparameter combination.

```python
# Loop to find the smallest validation loss
for i in range(N):
```
Inside the loop, at each iteration step, a new batch of random hyperparameters
will be generated. They are fed into the keras model and later saved, if
they are considered good enough.


```python
    # Generate random Hyperparameters
    neurons = 128 # kept constant as required
    activfunc = rnd.choice(['relu', 'sigmoid', 'tanh', 'elu', 'selu'])
    learningrate = rnd.choice([0.0001, 0.001, 0.01, 0,1])
    dropout = rnd.choice([0.1, 0.2, 0.3, 0.4, 0.5])
    batchsize = rnd.choice([8, 16, 32, 64, 128, 256, 512])
```

This part corresponds to the keras model given in the task.
It consists of one hidden layer with 128 neurons and one output layer with 10 neurons.

Epochs, neurons and the validation split are not part of this experiment and
are kept constant during all the iterations.

```python
    # build model
    model = models.Sequential([
        layers.Dense(neurons, input_shape=(784,)),
        layers.Activation(activfunc),
        layers.Dropout(dropout),
        layers.Dense(10),
        layers.Activation('softmax')])

    # compile model
    model.compile(
        loss='categorical_crossentropy',
        optimizer=keras.optimizers.Adam(lr=learningrate),
        metrics=['accuracy'])

    # train model
    results = model.fit(
        X_train, Y_train,
        batch_size=batchsize,
        epochs=10,
        verbose=2,
        validation_split=0.1)
```

In the last part of the loop, an evaluation takes place.
If the current validation loss is smaller then the previous, it gets saved along
with the number of the iteration of the loop where it occured and the
corresponding hyperparameters. The data gets overwritten each time the
conditions are met.
In retrospect, the saving of all the results of the iterations and hyperparameter
combinations in a file would have been better for an in-depth evaluation.
Because of the duration of each experiment (~30min), it is not possible for
us to change the code and repeat the experiments a sufficient time.

```python

    val_loss_list = results.history['val_loss']
    val_loss  =  val_loss_list[-1]
    # save new values and iteration if the validation loss is smaller than the previous
    if val_loss_best &gt; val_loss:
        best = i
        val_loss_best = val_loss
        parameters = [neurons, activfunc, learningrate, dropout, batchsize]
    val_loss_list = []

    #reinitialize the model
    keras.backend.clear_session()
```


## task2.2. How many trials are necessary to find a good combination of hyperparameters? [4 P] ##

The following figure shows the number of iterations to needed to achieve the best validation loss and therefore find the
best combination of hyperparameters. The median values is 26 iteration with an extension to 11 for the first quantille and
an extenssion to 33 iterations for the third quantille.
Min and max outliners can be found already after 1 iterations or at iteration 42 out of 50 iterations.

![plot: iterations](https://www.comet.ml/api/image/notes/download?imageId=hU1nns8fAetnzLlxclsal0o25&amp;objectId=02fc3416b6ef4c529d92b448932d7b1e)

This plot visualizes the raw data of the measurements.


![plot: validation loss vs no. of iterations, histogram](https://www.comet.ml/api/image/notes/download?imageId=yiYrkbl725LQ0zd7bkxufgEFv&amp;objectId=02fc3416b6ef4c529d92b448932d7b1e)

When looking at the activation function, it appears that the sigmoid activation function is predominant (40%) when looking for the best validation loss.


![plot: piechart: share of activation functions](https://www.comet.ml/api/image/notes/download?imageId=PuAOcWuWQ6b2ujoGtA8WQt1oj&amp;objectId=02fc3416b6ef4c529d92b448932d7b1e)

The next figure shows that the sigmoid function also returns the lowest validation loss with little variance (median: 0.069, q1:0.0685, q3: 0.0705).


![plot: validation loss vs activation function](https://www.comet.ml/api/image/notes/download?imageId=LXXhGvkTx3c3e9VtITNkXA3Fy&amp;objectId=02fc3416b6ef4c529d92b448932d7b1e)

The next plot shows the repartition of the batch size. It appears that the best results are obtained with a lower batch size (8).


![plot: validation loss vs batchsize](https://www.comet.ml/api/image/notes/download?imageId=WDJjJFMWND1ULZGNz10aRlS5B&amp;objectId=02fc3416b6ef4c529d92b448932d7b1e)

For the dropout parameter, wqe get the best results for a dropout value of 0.3. although the differences with the values of 0.1 and 0.2 are minimal.


![plot: validation loss vs dropout](https://www.comet.ml/api/image/notes/download?imageId=tmbbajhYnaPGthd2FljJoHJna&amp;objectId=02fc3416b6ef4c529d92b448932d7b1e)

The learning rate seems a bit more complex to analyse; althought the a learning rate of 0.001 is overrepresented, the learning rate of 0.01 yields ot a better result.
During the programmation, an error was introduced which could affect the results of the experiment. Instead of introducing the possible value of 0.1 to the random choice, the values 0 and 1 where introduced which lead to a bad validation loss.

```python
rnd.choice([0.0001 0.001 0.01, 0,1]) # komma instead of point for the last item, should be 0.1
```

plot: validation loss vs learning rate
![plot: validation loss vs learning rate](https://www.comet.ml/api/image/notes/download?imageId=qS1wP9m4vNJlgBJ76ssNNdSBD&amp;objectId=02fc3416b6ef4c529d92b448932d7b1e)




## task2.3. State the best values for hyperparameters. [1 P] ##
The following table gives the ranking of the 3 best combinations of hyperparameter

|Ranking| Activation Function| Learning rate| Dropout| batchsize|
|---------|---------|---------|---------|---------|
|1| relu | 0.001 | 0.3 | 64 |
|2| elu | 0.001 | 0.2 | 32 |
|3| sigmoid | 0.001 | 0.2 | 8 |
## task2.4. Give the validation and test error for these best values. [2 P] ##
The validation loss is defined as the absolute value of the training minus the validation loss. Unfortunately, we didn't logged the test loss
with comet, therefore we cannot provide it.
The calculated validation loss for the best combination of hyperparameter is 0.016867706.

# References #

  * [1] RWTH DLiPR SS20 Part 3: Optimization and Hyperparameter Tuning. Lecture Slides. 2020.
  * [2] deeplearning.ai. Hyperparameters tuning in practice: Pandas vs. Caviar. Date Unknown. URL: https://www.coursera.org/lecture/deep-neural-network/hyperparameters-tuning-in-practice-pandas-vs-caviar-DHNcc .
  * [3] machinelearningmastery.com. How to grid search hyperparameters for deep learning models in Python with Keras. 2018. URL: https://machinelearningmastery.com/grid-search-hyperparameters-deep-learning-models-python-keras/ .
  * [4] meenavyas.wordpress.com. Simple Neural Network Model using Keras and Grid Search HyperParametersTuning. 2018. URL: https://meenavyas.wordpress.com/2018/01/28/simple-neural-network-model-using-keras-and-grid-search-hyperparameterstuning/ .
  * [5] blog.floydhub.com. Practical Guide to Hyperparameters Optimization for Deep Learning Models. 2018. URL: https://blog.floydhub.com/guide-to-hyperparameters-search-for-deep-learning-models/ .
  * [6] blogtensorflow.org. Hyperparameter tuning with Keras Tuner. 2020. URL: https://blog.tensorflow.org/2020/01/hyperparameter-tuning-with-keras-tuner.html .
  * [7] comet.ml. Comet.ml Hyperparameter Optimization. Date Unknown. URL: https://www.comet.ml/parameter-optimization .
  * [8] wikipedia.org. Hyperparameter optimization. 2020-03-31. URL: https://en.wikipedia.org/wiki/Hyperparameter_optimization .
  * [9] macmachinelearningmastery.com. Dropout Regularization in Deep Learning Models With Keras. 2019-10-13. URL: https://machinelearningmastery.com/dropout-regularization-deep-learning-models-keras/
